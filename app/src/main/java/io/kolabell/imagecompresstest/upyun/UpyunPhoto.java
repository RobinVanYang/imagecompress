package io.kolabell.imagecompresstest.upyun;

import android.net.Uri;

import java.io.File;

/**
 * Created by kenliu on 16/7/18.
 * 拍照  相册选择
 */
public class UpyunPhoto {
    /**
     * 快速上传照片定义的内容
     */

    public static final int REQUEST_CODE = 732;
    /**
     * 又拍云传图片变量定义
     */

    public static final String DEFAULT_DOMAIN = "up-test1.b0.upaiyun.com";// 空间域名
    public static final String API_KEY = "h9BZiF2/YgHwAxNMIjy6CmbAi9M="; // 表单api验证密钥
    public static final String BUCKET = "up-test1";// 空间名称
    public static final long EXPIRATION = System.currentTimeMillis() / 1000 + 1000 * 5 * 10; // 过期时间，必须大于当前时间

    /**
     * 使用照相机拍照获取图片
     */
    public static final int SELECT_PIC_BY_TACK_PHOTO = 1;

    public static final int SELECT_PIC_BY_PICK_PHOTO = 2;
    /**
     * 使用相册中的图片
     */
    public static File mCurrentPhotoFile;
    public static Uri photoUri;// 照相之后的数据



}
