package io.kolabell.imagecompresstest;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;


import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import io.github.robinvanyang.imagecompress.Compressor;
import io.kolabell.imagecompresstest.upyun.UpYunException;
import io.kolabell.imagecompresstest.upyun.UpYunUtils;
import io.kolabell.imagecompresstest.upyun.Uploader;
import io.kolabell.imagecompresstest.upyun.UpyunPhoto;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;
import me.nereo.multi_image_selector.MultiImageSelector;
import me.nereo.multi_image_selector.MultiImageSelectorActivity;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static final int REQUEST_CODE_IMAGE = 1;

    @BindView(R.id.imageView1) ImageView mImageView1;

    private ArrayList<String> mFileList;
    ProgressDialog mDialog;
    final Compressor compressor = new Compressor();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        mFileList = getImageList();
        mDialog = new ProgressDialog(this);
        mDialog.setTitle("loading");
    }

    private ArrayList<String> getImageList() {
        ArrayList<String> imagePathList = new ArrayList<>();
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_093623.jpg"); //7998832
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_093613.jpg"); //8747703
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_090621.jpg"); //10120000
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_085213.jpg"); //9473617
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_083832.jpg"); //6326927
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_083823.jpg"); //7259636
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_082804.jpg"); //9533124
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20170206_082640.jpg"); //9346667
        imagePathList.add("/storage/emulated/0/DCIM/Camera/IMG_20161214_113501.jpg"); //7830202
        return imagePathList;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case REQUEST_CODE_IMAGE:
                    mFileList = data.getStringArrayListExtra(MultiImageSelectorActivity.EXTRA_RESULT);
                    for (String s : mFileList) {
                        printImageInfo(s);
                    }
                    break;
                default:
                    break;
            }
        }
    }

    @OnClick({R.id.btn_scale_by_insamplesize, R.id.btn_scale, R.id.btn_read_angle, R.id.btn_fresco})
    public void onViewClick(View view) {
        switch (view.getId()) {
            case R.id.btn_scale_by_insamplesize:
                scaleByInSampleSize();
                break;
            case R.id.btn_scale:
                compressor.reduceImageResolution(mFileList.get(0));
                break;
            case R.id.btn_read_angle:
                int rotateDegree = getImageDegree(mFileList.get(0));
                Log.d(TAG, "image angle: " + rotateDegree);
                break;
            case R.id.btn_fresco:
                func1(mFileList);
                break;
            default:
                break;
        }
    }

    private void scaleByInSampleSize() {
        String imagePath = mFileList.get(0);
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imagePath, options);
        int inSampleSize = Compressor.calculateInSampleSize(options);

        compressor.decodeFile(mFileList.get(0), inSampleSize);
    }

    private void func1(final ArrayList<String> imageList) {
        showDialog();
        startTime = System.currentTimeMillis();


        compressor.compressImage(imageList).concatMapEager(new Function<String, ObservableSource<String>>() {
                    @Override
                    public ObservableSource<String> apply(final @NonNull String path) throws Exception {
                        return Observable.create(new ObservableOnSubscribe<String>() {
                            @Override
                            public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                                Log.d(TAG, "upload: " + Thread.currentThread().getName());
                                uploadToServer(path);
                                e.onNext(path);
                                e.onComplete();
                            }
                        }).subscribeOn(Schedulers.io());
                    }
                })
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<String>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onNext(String s) {
                        Log.d(TAG, "next: " + s);
                    }

                    @Override
                    public void onError(Throwable throwable) {
                        throwable.printStackTrace();
                    }

                    @Override
                    public void onComplete() {
                        hideDialog();
                        Log.d(TAG, "完成, 耗时: " + (System.currentTimeMillis() - startTime));
                        Toast.makeText(MainActivity.this, "完成, 耗时: " + (System.currentTimeMillis() - startTime), Toast.LENGTH_LONG).show();
                    }
                });
    }

    private void uploadToServer(String path) {
        try {
            File file = new File(path);
            // 设置服务器上保存文件的目录和文件名，如果服务器上同目录下已经有同名文件会被自动覆盖的。
            String SAVE_KEY = File.separator + "ysc_test/" + file.getName();

            // 取得base64编码后的policy
            String policy = UpYunUtils.makePolicy(SAVE_KEY, UpyunPhoto.EXPIRATION, UpyunPhoto.BUCKET);

            // 根据表单api签名密钥对policy进行签名
            // 通常我们建议这一步在用户自己的服务器上进行，并通过http请求取得签名后的结果。
            String signature = UpYunUtils.signature(policy + "&" + UpyunPhoto.API_KEY);

            // 上传文件到对应的bucket中去。

            Uploader.upload(policy, signature, UpyunPhoto.BUCKET, path);

        } catch (UpYunException e) {
            e.printStackTrace();
        }
    }

    private int getImageDegree(String imagePath) {
        try {
            ExifInterface exif = new ExifInterface(imagePath);
            int rotation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            return exifToDegrees(rotation);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private static int exifToDegrees(int exifOrientation) {
        if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_90) { return 90; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_180) {  return 180; }
        else if (exifOrientation == ExifInterface.ORIENTATION_ROTATE_270) {  return 270; }
        return 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add:
                selectPicture();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private void selectPicture() {
        MultiImageSelector.create().start(this, REQUEST_CODE_IMAGE);
    }

    private void printImageInfo(String imageFilePath) {
        File file = new File(imageFilePath);
        L.d(imageFilePath);
        L.d(file.length());
        if (file.exists()) {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeFile(imageFilePath, options);
            } catch (Exception e) {
            }
        }
    }

    long startTime;


    public void showDialog() {
        mDialog.show();
    }

    public void hideDialog() {
        if (mDialog.isShowing()) {
            mDialog.hide();
        }
    }
}
