package io.kolabell.imagecompresstest;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * @author RobinVanYang
 * @createTime 2017-02-14 15:03
 */

public class ImageListAdapter extends RecyclerView.Adapter<ImageListAdapter.ImageViewHolder> {
    private ArrayList<String> mDataSource = new ArrayList<>();

    @Override
    public ImageListAdapter.ImageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.view_image, parent, false);
        return new ImageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ImageListAdapter.ImageViewHolder holder, int position) {
        holder.bindData(getItem(position));
    }

    public void addListData(ArrayList<String> items) {
        mDataSource.addAll(items);
        notifyDataSetChanged();
    }

    private String getItem(int position) {
        return mDataSource.get(position);
    }

    @Override
    public int getItemCount() {
        return mDataSource.size();
    }

    static class ImageViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_image) ImageView mIvImage;

        public ImageViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(String imageUrl) {
            Picasso.with(itemView.getContext()).load("file://" + imageUrl).fit().centerCrop().into(mIvImage);
        }
    }
}
