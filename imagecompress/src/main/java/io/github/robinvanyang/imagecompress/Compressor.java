package io.github.robinvanyang.imagecompress;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.media.ExifInterface;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.ArrayList;

import io.github.robinvanyang.imagecompress.nativecode.ImageCompressNativeLoader;
import io.github.robinvanyang.imagecompress.nativecode.JpegTranscoder;
import io.github.robinvanyang.imagecompress.nativecode.internal.DoNotStrip;
import io.github.robinvanyang.imagecompress.nativecode.util.FileUtil;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.ObservableSource;
import io.reactivex.annotations.NonNull;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

/**
 * 压缩图片类
 *
 * @author RobinVanYang
 * @createTime 2017-05-15 17:54
 */

public class Compressor {
    private static final String TAG = "Compressor";
    private static final int DEFAULT_QUALITY = 80;
    private static String DEFAULT_DISK_CACHE_DIR = "upbox/compressed_image";
    private static int MIN_COMPRESS_SIZE = 100 * 1024;
    private static final int WIDE_MODE_HEIGHT_CRITICAL_VALUE = 540;
    private static final int WIDTH_ONE = 720;
    private static final int WIDTH_TWO = 1080;

    static {
        ImageCompressNativeLoader.load();
    }

    public Observable<String> compressImage(final ArrayList<String> imageList) {
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                for (String str : imageList) {
                    e.onNext(str);
                }
                e.onComplete();
            }
        })
                .observeOn(Schedulers.computation())
                .concatMap(new Function<String, ObservableSource<String>>() {
                    @Override
                    public ObservableSource<String> apply(final @NonNull String image) throws Exception {
                        return Observable.create(new ObservableOnSubscribe<String>() {
                            @Override
                            public void subscribe(@NonNull ObservableEmitter<String> e) throws Exception {
                                Log.d(TAG, "compressImage: " + Thread.currentThread().getName());
                                e.onNext(compress(image, 1));
                                e.onComplete();
                            }
                        });
                    }
                });
    }

    public Observable<String> compressImage(final String imageFilePath) {
        return Observable.create(new ObservableOnSubscribe<String>() {
            @Override
            public void subscribe(ObservableEmitter<String> e) throws Exception {
                Log.d(TAG, "compressImage: " + Thread.currentThread().getName());
                e.onNext(compress(imageFilePath, 1));
                e.onComplete();
            }
        });
    }

    private String compress(final String imageFilePath, int multiCompressInterval) throws Exception {
        String dstImgPath;
        if (imageFilePath.toLowerCase().endsWith(".png")) {

            if (multiCompressInterval > 0) {
                Thread.sleep(multiCompressInterval);
            }

            dstImgPath = compressPng(imageFilePath);
        } else {
            dstImgPath = getPhotoCacheDir(DEFAULT_DISK_CACHE_DIR)
                    + File.separator + System.currentTimeMillis() + ".jpg";

            if (multiCompressInterval > 0) {
                Thread.sleep(multiCompressInterval);
            }

            compressJpg(imageFilePath, dstImgPath);
        }

        return dstImgPath;
    }

    /**
     * user fresco's compress algorithm.
     *
     * @param src
     * @param dst
     */
    private void compressJpg(String src, String dst) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = new FileInputStream(src);

            outputStream = new FileOutputStream(dst);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            BitmapFactory.decodeFile(src, options);

            int inSampleSize = Compressor.calculateInSampleSize(options);
            int quality = 50;
            JpegTranscoder.transcodeJpeg(inputStream, outputStream, 0, 8 / inSampleSize, quality);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != inputStream) try {
                inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (null != outputStream) try {
                outputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private String compressPng(String imgPath) {
        String dstImgPath = getPhotoCacheDir(DEFAULT_DISK_CACHE_DIR)
                + File.separator + System.currentTimeMillis() + ".jpg";

        File srcFile = new File(imgPath);
        if (!srcFile.exists() || srcFile.isDirectory()) {
            return null;
        }

        //源图片小于100K时，直接拷贝为目标文件
        if (srcFile.length() < MIN_COMPRESS_SIZE) {
            try {
                FileUtil.copy(srcFile, new File(dstImgPath));
            } catch (IOException e) {
                e.printStackTrace();
            }
            return dstImgPath;
        }


        //长宽等比缩放
        Bitmap bitmap = reduceImageResolution(imgPath);

        qualityCompress(bitmap, dstImgPath);
        return dstImgPath;
    }

    /**
     * 等比对图片长宽进行缩放
     *
     * @param imgPath
     * @return
     */
    public Bitmap reduceImageResolution(String imgPath) {
        File file = new File(imgPath);
        if (!file.exists()) return null;

        Bitmap bitmap = null;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(imgPath, options);


        int inSampleSize = calculateInSampleSize(options);

        bitmap = decodeFile(imgPath, inSampleSize);

        printImageInfo(bitmap);

        if (shouldDealInWideMode(options)) {
            return createScaledBitmapInWideMode(bitmap, options.outWidth, options.outHeight);
        } else {
            return createScaledBitmapInNormalMode(bitmap, options.outWidth, options.outHeight);
        }
    }

    private Bitmap createScaledBitmapInNormalMode(Bitmap src, int originalWidth, int originalHeight) {
        /* (0, 720] or 1280 */
        if (originalWidth <= WIDTH_ONE || originalWidth == WIDTH_TWO) {
            return src;
        }
        /* (720, 1280), (1280, ∞) */
        else {
            int dstWidth;
            if (originalWidth > WIDTH_TWO) {
                dstWidth = WIDTH_TWO;
            } else {
                dstWidth = WIDTH_ONE;
            }
            float scaleRatio = (float) dstWidth / originalWidth;
            int dstHeight = (int) (originalHeight * scaleRatio);

            return createScaledBitmap(src, dstWidth, dstHeight);
        }
    }

    private Bitmap createScaledBitmapInWideMode(Bitmap src, int originalWidth, int originalHeight) {
        if (originalHeight <= WIDE_MODE_HEIGHT_CRITICAL_VALUE) {
            return src;
        } else {
            int dstHeight = WIDE_MODE_HEIGHT_CRITICAL_VALUE;
            float scaleRatio = (float) dstHeight / originalHeight;
            int dstWidth = (int) (originalWidth * scaleRatio);

            return createScaledBitmap(src, dstWidth, dstHeight);
        }
    }

    private Bitmap createScaledBitmap(Bitmap src, int dstWidth, int dstHeight) {
        Bitmap dst = Bitmap.createScaledBitmap(src, dstWidth, dstHeight, false);

        if (src != dst) {
            src.recycle();
            src = null;
        }

        printImageInfo(dst);
        return dst;
    }

    private String saveBitmapToFile(Bitmap bitmap, String compressedFilePath) {
        if (TextUtils.isEmpty(compressedFilePath)) return null;

        FileOutputStream out = null;
        try {
            out = new FileOutputStream(compressedFilePath);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
            return compressedFilePath;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (null != out) try {
                out.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            bitmap.recycle();
            bitmap = null;
        }
        return null;
    }

    /**
     * 质量压缩. 如果bitmap大小达到最小压缩临界值时，直接保存.
     * 否则调用lib-jpeg,进行质量压缩.
     * 逻辑为: 如果位图大小小于2M时，则使用较高质量(80)进行压缩, 如果大于2M，则使用50的quality来进行压缩
     *
     * @param bitmap
     * @param dstImgPath
     */
    private void qualityCompress(Bitmap bitmap, String dstImgPath) {
        //长宽缩放之后的处理，可直接保存，也可能需要做质量压缩.
        if (null != bitmap) {
            //当bitmap大小达到最小需压缩图片大小时，直接保存
            if (bitmap.getByteCount() < MIN_COMPRESS_SIZE) {
                saveBitmapToFile(bitmap, dstImgPath);
            }
            //调用lib-jpeg，进行质量压缩
            else {
                if (bitmap.getByteCount() < 1024 * 1024 * 2) {
                    Compressor.compressBitmap(bitmap, dstImgPath, true);
                } else {
                    Compressor.compressBitmap(bitmap, 50, dstImgPath, true);
                }
                bitmap.recycle();
                bitmap = null;
            }
        }
    }

    public Bitmap decodeFile(String imgPath, int inSampleSize) {

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;

        BitmapFactory.decodeFile(imgPath, options);

        if (inSampleSize > 1) {
            options.inSampleSize = inSampleSize;
        } else {
            options.inSampleSize = 1;
        }

        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(imgPath, options);
    }


    /**
     * calculate the possible inSampleSize(power of 2), if the inSampleSize greater than 1,
     * that means should go to the first scale by BitmapFactory.decodeFile.
     *
     * @param options
     * @return
     */
    public static int calculateInSampleSize(BitmapFactory.Options options) {
        final int width = options.outWidth;
        final int height = options.outHeight;
        int inSampleSize = 1;

        if (shouldDealInWideMode(options)) {
            inSampleSize = maxPowerExponent(WIDE_MODE_HEIGHT_CRITICAL_VALUE, height);
        } else if (width >= WIDTH_TWO * 2) {
            inSampleSize = maxPowerExponent(WIDTH_TWO, width);
        } else if (width >= WIDTH_ONE * 2) {
            inSampleSize = maxPowerExponent(WIDTH_ONE, width);
        }

        return inSampleSize;
    }

    private static boolean shouldDealInWideMode(BitmapFactory.Options options) {
        return ((float) options.outWidth / options.outHeight) > 2.0 && options.outHeight > WIDE_MODE_HEIGHT_CRITICAL_VALUE;
    }

    /**
     * pow(base, n) <= value, get the max n.
     *
     * @return
     */
    private static int maxPowerExponent(int base, int value) {
        int n = 1;

        final float halfValue = (float) value / 2;  //1728
        if (value >= base * 2) {
            while (halfValue / n >= base) {
                n *= 2;
            }
        }

        return n;
    }

    /**
     * 读取图片属性：旋转的角度
     *
     * @param path 图片绝对路径
     * @return degree旋转的角度
     */
    public static int readPictureDegree(String path) {
        int degree = 0;
        try {
            ExifInterface exifInterface = new ExifInterface(path);
            int orientation = exifInterface.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    degree = 90;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    degree = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    degree = 270;
                    break;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return degree;
    }

    public String getReadableFileSize(long size) {
        if (size <= 0) {
            return "0";
        }
        final String[] units = new String[]{"B", "KB", "MB", "GB", "TB"};
        int digitGroups = (int) (Math.log10(size) / Math.log10(1024));
        return new DecimalFormat("#,##0.#").format(size / Math.pow(1024, digitGroups)) + " " + units[digitGroups];
    }

    private void printImageInfo(Bitmap bitmap) {
//        Log.d(TAG, "image size: " + bitmap.getByteCount() / 1024 + "Kb");
//        Log.d(TAG, "bitmap size: " + bitmap.getWidth() + "/" + bitmap.getHeight());
    }

    /**
     * copied from https://github.com/Curzibn/Luban/blob/master/library/src/main/java/top/zibin/luban/Luban.java
     *
     * @param cacheName
     * @return
     */
    private File getPhotoCacheDir(String cacheName) {
        File cacheDir = Environment.getExternalStorageDirectory();
        if (cacheDir != null) {
            File result = new File(cacheDir, cacheName);
            if (!result.mkdirs() && (!result.exists() || !result.isDirectory())) {
                // File wasn't able to create a directory, or the result exists but not a directory
                return null;
            }

            File noMedia = new File(cacheDir + "/.nomedia");
            if (!noMedia.mkdirs() && (!noMedia.exists() || !noMedia.isDirectory())) {
                return null;
            }

            return result;
        }

        return null;
    }

    public static void compressBitmap(Bitmap bit, String fileName,
                                      boolean optimize) {
        compressBitmap(bit, DEFAULT_QUALITY, fileName, optimize);

    }

    public static void compressBitmap(Bitmap bit, int quality, String fileName,
                                      boolean optimize) {
        Log.d("native", "compressPng of native");
        if (bit.getConfig() != Bitmap.Config.ARGB_8888) {
            Bitmap result = null;

            result = Bitmap.createBitmap(bit.getWidth(), bit.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);
            Rect rect = new Rect(0, 0, bit.getWidth(), bit.getHeight());
            canvas.drawBitmap(bit, null, rect, null);
            saveBitmap(result, quality, fileName, optimize);
        } else {
            saveBitmap(bit, quality, fileName, optimize);
        }
    }

    public static void compressBitmap(Bitmap bit, int quality, String fileName, int outerWidth, int outerHeight,
                                      boolean optimize) {
        Log.d("native", "compressPng of native");
        if (bit.getConfig() != Bitmap.Config.ARGB_8888) {
            Bitmap result = null;

            result = Bitmap.createBitmap(bit.getWidth(), bit.getHeight(),
                    Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(result);
            Rect rect = new Rect(0, 0, bit.getWidth(), bit.getHeight());
            canvas.drawBitmap(bit, null, rect, null);
            saveBitmap(result, quality, fileName, outerWidth, outerHeight, optimize);
            result.recycle();
        } else {
            saveBitmap(bit, quality, fileName, outerWidth, outerHeight, optimize);
        }
    }

    private static void saveBitmap(Bitmap bit, int quality, String fileName,
                                   boolean optimize) {
        nativeCompressBitmap(bit, bit.getWidth(), bit.getHeight(), quality,
                fileName.getBytes(), optimize);
    }

    private static void saveBitmap(Bitmap bit, int quality, String fileName, int outerWidth, int outerHeight,
                                   boolean optimize) {
        nativeCompressBitmap(bit, outerWidth, outerHeight, quality,
                fileName.getBytes(), optimize);
    }

    @DoNotStrip
    private static native String nativeCompressBitmap(Bitmap bit, int w, int h,
                                                      int quality, byte[] fileNameBytes, boolean optimize);
}
