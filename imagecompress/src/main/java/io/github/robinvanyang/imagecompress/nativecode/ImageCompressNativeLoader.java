/*
 * Copyright (c) 2015-present, Facebook, Inc.
 * All rights reserved.
 *
 * This source code is licensed under the BSD-style license found in the
 * LICENSE file in the root directory of this source tree. An additional grant
 * of patent rights can be found in the PATENTS file in the same directory.
 */

package io.github.robinvanyang.imagecompress.nativecode;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.github.robinvanyang.imagecompress.nativecode.soloader.SoLoaderShim;


/**
 * Single place responsible for loading libimagepipeline.so and its dependencies.
 *
 * If your class has a native method whose implementation lives in libimagepipeline.so then call
 * {@link ImageCompressNativeLoader#load} in its static initializer:
 * <code>
 *   public class ClassWithNativeMethod {
 *     static {
 *       ImageCompressNativeLoader.load();
 *     }
 *
 *     private static native void aNativeMethod();
 *   }
 * </code>
 */
public class ImageCompressNativeLoader {
  public static final String DSO_NAME = "imagecompress";

  public static final List<String> DEPENDENCIES;
  static {
    List<String> dependencies = new ArrayList<String>();
    DEPENDENCIES = Collections.unmodifiableList(dependencies);
  }

  public static void load() {
    SoLoaderShim.loadLibrary(DSO_NAME);
  }
}
